# Project 5: Brevet time calculator with Ajax and MongoDB

Author: Delaney Carleton
Email: dcarlet2@uoregon.edu

#The objective of Project 5 is:
Simple list of controle times from project 4 stored in MongoDB database.

A minimal implementation of Docker compose in DockerMongo folder, using which you can connect the flask app to MongoDD.

This project, you will create the following functionality: 
    
    1) Create two buttons ("Submit") and ("Display") in the page where have controle times. 
    
    2) On clicking the Submit button, the control times should be entered into the database. 

    3) On clicking the Display button, the entries from the database should be displayed in a new page. 

Error cases will be handled appropriately. If there are no entries submitted then an error will pop up. Along with this, if a page is not present then an error will pop up as well.

Test cases will be present in order to test the functionality of the two buttons

